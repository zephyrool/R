# install packages R script for standard packages
# test edit
install.packages("car", repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"foreign"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"estout"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"lmtest"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"lattice"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"latticeExtra"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"mixreg"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"mlogit"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"R.oo"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"quadprog"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"HFWutils"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"BayesX"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"BayesTree"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"MCMCpack"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"tseries"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"maptools"	, repos="http://cran.ma.imperial.ac.uk" )

install.packages("car", repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"foreign"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"estout"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"lmtest"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"lattice"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"latticeExtra"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"mixreg"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"mlogit"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"R.oo"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"quadprog"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"HFWutils"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"BayesX"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"BayesTree"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"MCMCpack"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"tseries"	, repos="http://cran.ma.imperial.ac.uk" )
install.packages(	"maptools"	, repos="http://cran.ma.imperial.ac.uk" )



paclist = "car"
paclist = c(paclist, "foreign")
paclist = c(paclist, "estout")
paclist = c(paclist, "lmtest")
paclist = c(paclist, "lattice")
paclist = c(paclist, "latticeExtra")
paclist = c(paclist, "mixreg")
paclist = c(paclist, "mlogit")
paclist = c(paclist, "R.oo")
paclist = c(paclist, "quadprog")
paclist = c(paclist, "HFWutils")
paclist = c(paclist, "BayesX")
paclist = c(paclist, "BayesTree")
paclist = c(paclist, "MCMCpack")
paclist = c(paclist, "tseries")
paclist = c(paclist, "maptools")

paclist = c(paclist, "odbc")
paclist = c(paclist, "RMariaDB")
paclist = c(paclist, "Hmisc")
paclist = c(paclist, "pastecs")
paclist = c(paclist, "doBy")
paclist = c(paclist, "lubridate")
paclist = c(paclist, "hexbin")
paclist = c(paclist, "rio")
paclist = c(paclist, "curl")
paclist = c(paclist, "ggplot2")
paclist = c(paclist, "vcd")
paclist = c(paclist, "cowplot")

install.packages(paclist, repos="http://cran.ma.imperial.ac.uk")



########################################################
# things to install in linux for setting up mariadb sql
########################################################
sudo apt-get install  unixodbc-dev
sudo apt-get install libmariadbclient18
sudo apt-get install libmariadb-dev
sudo apt-get install libmariadbclient-dev
sudo apt-get install libmariadb-client-lgpl-dev
libmysqlclient-dev
libcurl4-openssl-dev
r-base
r-base-dev
########################################################

install.packages("odbc", repos="http://cran.ma.imperial.ac.uk" )
install.packages("RMariaDB", repos="http://cran.ma.imperial.ac.uk")
